//-*- c++ -*-
// -------------------------------------------
// RCS data:
// $Date: 2003/06/23 14:47:22 $
// $Revision: 1.1.1.1 $
// $Source: /cvsroot/miwm/miwm/miwm/cursor.cc,v $
// $Id: cursor.cc,v 1.1.1.1 2003/06/23 14:47:22 bwise837 Exp $
// $RCSfile: cursor.cc,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------

#include	<stdio.h>

#include	<X11/X.h>
#include	<X11/Xlib.h>
#include	<X11/Xutil.h>
#include	<X11/cursorfont.h>

#include "miwm.h"

Cursor root_cursor;
Cursor frame_cursor;
Cursor menu_cursor;

static struct CursorMapping {
  Edge		edge;
  int		font_char;
  Cursor	cursor;
} cursor_map[] = {

  /*
	 * These *MUST* be in the same order as they appear in miwm.h, because
	 * we index rather than search in edgecursor() later on.
	 */

  {ETopLeft,		XC_top_left_corner, 0,},
  {ETop,			XC_top_side, 0,},
  {ETopRight,		XC_top_right_corner, 0,},
  {ERight,			XC_right_side, 0,},
  {ENone,			XC_fleur, 0,},
  {ELeft,			XC_left_side, 0,},
  {EBottomLeft,	XC_bottom_left_corner, 0,},
  {EBottom,		XC_bottom_side, 0,},
  {EBottomRight,	XC_bottom_right_corner, 0,},

  {ENone,			0, 0,},
};

extern void
initCursor(void) {
  //   XColor red;
  XColor exact;
  Colormap cmp;
  int i;
  // XC_crosshair, XC_X_cursor, XC_fleur are all reasonable for BPW
  cmp = DefaultColormap(dpy, DefaultScreen(dpy));
//   root_cursor = XCreateFontCursor(dpy, XC_X_cursor);
  root_cursor = XCreateFontCursor(dpy, XC_X_cursor);
  frame_cursor = XCreateFontCursor(dpy, XC_left_ptr);
  menu_cursor = XCreateFontCursor(dpy, XC_crosshair);

//   XRecolorCursor(dpy, root_cursor, &red, &exact);
  XRecolorCursor(dpy, root_cursor, &theWM->rootCursorColor, &exact);
  XRecolorCursor(dpy, menu_cursor, &theWM->frameCursorColor, &exact);
  XRecolorCursor(dpy, frame_cursor, &theWM->frameCursorColor, &exact);

  for (i = 0; cursor_map[i].font_char != 0; i++) {
    cursor_map[i].cursor = XCreateFontCursor(dpy, cursor_map[i].font_char);
    XRecolorCursor(dpy, cursor_map[i].cursor, &theWM->frameCursorColor, &exact);
  }
//   cout << "init cursor finished" << endl << flush;
  return;
}

extern Cursor
edgecursor(Edge edge) {
  return cursor_map[(int)edge].cursor;
}


// -------------------------------------------
// end of cursor.cc
// -------------------------------------------
