//-*- c++ -*-
// -------------------------------------------
// RCS data:
// $Date: 2003/06/23 14:47:22 $
// $Revision: 1.1.1.1 $
// $Source: /cvsroot/miwm/miwm/miwm/alpha.cc,v $
// $Id: alpha.cc,v 1.1.1.1 2003/06/23 14:47:22 bwise837 Exp $
// $RCSfile: alpha.cc,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------

#include "alpha.h"

// ----------------------------------
ostream& operator << (ostream& s, TLogical r) {
  switch (r) {
  case tbTrue:
    s << "True";
    break;
  case tbFalse:
    s << "False";
    break;
  case tbUnknown:
    s << "Unknown";
    break;
  };
  return s;
}; 

// ----------------------------------
double
dabs(double x)
{
  if (x<0)
    return -x;
  else
    return x;
}

int 
iabs (int x)
{
  if (x<0)
    return -x;
  else
    return x;
}

int
imax (int a, int b)
{
  int result  = a;
  if (b > a)
    result = b;
  return result;
}

int
imin (int a, int b)
{
  int result  = a;
  if (b < a)
    result = b;
  return result;
}

double
dmax (double a, double b)
{
  float result = a;
  if (b > a)
    result = b;
  return result;
}

double
dmin (double a, double b)
{
  float result = a;
  if (b < a)
    result = b;
  return result;
}

// ------------------------------------------

double
expt(double x, int n)
{
  double y = 1.0;
  assert (n >= 0);
  while (n>0)
    {
      y = y*x;
      n = n-1;
    }
  return y;
}

double
dexpt(double x, double e)
{
  double z = log(x);
  return exp(e*z);
}

int iround(double x)
{
  return ((int) round(x,0));  
}

double
round(double x, int n)
{
  assert (n >= 0);
  double y;
  double rslt;
  int xInteger = ((int) x);
  double xFraction = x - xInteger;

  if (n==0)
    y = 1.0;
  else
    y = expt(10.0, n);

  xFraction = ((int)(0.5+ y*xFraction ))/y;
  rslt = xInteger + xFraction;

  return rslt;
}


int ifloor(double x) {
  return ((int) floor(x));
}

// {
//   int j;
//   int i = (int)x;

//   if (x>=0)   // (int)(3.14) == 3, f(3.14) == 3
//     j = i;

//   if (x<0)   // (int)(-3.14) == -3, f(-3.14) == -4
//     {   
//       if (i>x)
// 	j = i-1;
//       else
// 	j = i;
//     }
//   return  j;
// }

int 
oddP(int x)
{
  return ( ( x % 2) == 1);
}

int 
evenP(int x)
{
  return ( ( x % 2) == 0);
}

double
delta(double a, double b)
{
  return (200.0 * dabs(a-b)) / (dabs(a) + dabs(b));
}
 int iceiling(double x)
 {
   int j = 0 ;
   int i = (int)x;

   if (x>=0)   // (int)(3.14) == 3, c(3.14) == 4
     {
       if (i<x)
 	j = i+1;
       else
 	j = i;
     }

   if (x<0)   // (int)(-3.14) == -3, c(-3.14) == -3
     j = i;

   return  j;
 }



 int gcd(int a, int b) {
//    cout << "gcd("<<a<<", "<< b<<")=";
   int c, e;
   int done = 0;
   e = a;
   while (0 == done) {
     if (a > b) {
       c = a;
       a = b;
       b = c;
     }
     assert (a <= b);
     if ((a == b) || (a == 0) || (a == 1)) {
       e = a;
       done = 1;
     }
     else
       b = b - a;
   }
//    cout << e << endl;
   return e;
}

// this occaisonally crashes with extremely deep recursion,
// so I had to replace it with the iterative version above.
/*
 int gcd(int a, int b)
 {
   int c, e;
   assert (a>=0);
   assert (b>=0);
   if (a>b)
     e =  gcd(b,a);
   else  if ((a==b) || (0 == a) || (1 == a))
     e = a;
   else {
     // a< b
     c = b-a;
  
     if (c<a)
       e = gcd(c,a);
     else
       e = gcd(a,c);
   }
   return e;
 }
*/
// ----------------------------------
// end of alpha.cc
// ----------------------------------

