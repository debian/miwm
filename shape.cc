//-*- c++ -*-
// -------------------------------------------
// RCS data:
// $Date: 2003/06/23 14:47:24 $
// $Revision: 1.1.1.1 $
// $Source: /cvsroot/miwm/miwm/miwm/shape.cc,v $
// $Id: shape.cc,v 1.1.1.1 2003/06/23 14:47:24 bwise837 Exp $
// $RCSfile: shape.cc,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------

#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>

#include	<unistd.h>

#include	<X11/X.h>
#include	<X11/Xlib.h>
#include	<X11/Xutil.h>
#ifdef SHAPE
#include	<X11/extensions/shape.h>
#endif

#include	"miwm.h"

/*ARGSUSED*/
extern void 
setshape(Client *c)
{
#ifdef  SHAPE
  int	n;
  int	order;
  XRectangle *rect;

  if (shape) {
    XShapeSelectInput(dpy, c->window, ShapeNotifyMask);
    rect = XShapeGetRectangles(dpy, c->window, ShapeBounding,
			       &n, &order);
    if (n > 1)
      XShapeCombineShape(dpy, c->parent, ShapeBounding,
			 border - 1, border - 1, c->window,
			 ShapeBounding, ShapeSet);

    XFree(rect);
  }
#else
#endif
}


#ifdef  SHAPE
static void
shapenotify(XEvent *ev)
{
  Client	*c;
  XShapeEvent	*e = (XShapeEvent *)ev;

  c = getClient(e->window);
  if (c != 0)
    setshape(c);
}
#endif

/*ARGSUSED*/
extern int
shapeevent(XEvent *ev)
{
#ifdef  SHAPE
  if (shape && ev->type == shape_event) {
    shapenotify(ev);
    return 1;
  }
#else
#endif
  return 0;
}

/*ARGSUSED*/
extern int
isshaped(Window w)
{
#ifdef SHAPE
  int	n;
  int	order;
  XRectangle	*rect;

  rect = XShapeGetRectangles(dpy, w, ShapeBounding, &n, &order);
  XFree(rect);

  return (n > 1);
#else
  return 0;
#endif
}



extern int
servershapes(void)
{
#ifdef SHAPE
  int shape_error;
  return XShapeQueryExtension(dpy, &shape_event, &shape_error);
#else
  return 0;
#endif
}

// -------------------------------------------
// end of shape.cc
// -------------------------------------------
