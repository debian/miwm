//-*- c++ -*-
// -------------------------------------------
// RCS data:
// $Date: 2003/07/04 16:43:44 $
// $Revision: 1.2 $
// $Source: /cvsroot/miwm/miwm/miwm/resource.cc,v $
// $Id: resource.cc,v 1.2 2003/07/04 16:43:44 sgbeal Exp $
// $RCSfile: resource.cc,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------
// read X resources
// -------------------------------------------

#include	<stdio.h>
#include	<stdlib.h>

#include	<unistd.h>
#include	<sys/types.h>
#include	<sys/wait.h>

#include	<X11/X.h>
#include	<X11/Xos.h>
#include	<X11/Xlib.h>
#include	<X11/Xresource.h>
#include	<X11/Xutil.h>
#include	<X11/Xatom.h>

#include	"miwm.h"
#include        "miwm_framework.h"

const char	*font_name;			/*	User's choice of titlebar font. */
const char	*popup_font_name;	/*	User's choice of menu font. */
// char	*btn1_command;		/*	User's choice of button 1 command. */
// char	*btn2_command;		/*	User's choice of button 2 command. */
int	border;				/*	User's choice of border size. */

char *
stringCopy(char *p)
{
  char	*s ;

//   s = (char*)malloc(strlen(p) + 1);
  s = new char[ sizeof(char) * (strlen(p) + 1) ];
  if(s == 0)
    miAbort("new fails");
  return strcpy(s, p);
}

extern void
getResources(void)
{
  XrmDatabase	db;
  XrmValue	value;
  char	*resource_manager;
  char	*type;

  /* Set our fall-back defaults. */
  font_name = miwm::config().getString( "default_title_font", DEFAULT_TITLE_FONT ).c_str();
  popup_font_name = miwm::config().getString( "default_popup_font", DEFAULT_POPUP_FONT ).c_str();
  // note: the font vars are not properly persistant: need to make accessor funcs and remove
  // these vars, so we can wrap the config file part of it there.

//   popup_font_name = SPECIAL_POPUP_FONT;
  border = miwm::config().getInt( "client_border_size", DEFAULT_BORDER );
//   btn1_command = 0;
//   btn2_command = DEFAULT_TERMINAL;

  resource_manager = XResourceManagerString(dpy);
  if (resource_manager == 0)
    return;

  XrmInitialize();
  db = XrmGetStringDatabase(resource_manager);
  if (db == 0)
    return;

  /* Fonts. */
  if (XrmGetResource(db, "miwm.titleFont", "Font", &type, &value) == True)
    if (strcmp(type, "String") == 0)
      font_name = stringCopy((char *) value.addr);
  if (XrmGetResource(db, "miwm.popupFont", "Font", &type, &value) == True)
    if (strcmp(type, "String") == 0)
      popup_font_name = stringCopy((char *) value.addr);

  /* Window border width. */
  if(XrmGetResource(db, "miwm.border", "Border", &type, &value) == True)
    if (strcmp(type, "String") == 0)
      border = (int) strtol((char *) value.addr, (char **) 0, 0);

//   /* The button commands. */
//   if (XrmGetResource(db, "miwm.button1", "Command", &type, &value) == True)
//     if (strcmp(type, "String") == 0)
//       btn1_command = stringCopy((char *) value.addr);
//   if (XrmGetResource(db, "miwm.button2", "Command", &type, &value) == True)
//     if (strcmp(type, "String") == 0)
//       btn2_command = stringCopy((char *) value.addr);
}


// -------------------------------------------
// end of resource.cc
// -------------------------------------------
