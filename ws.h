//-*-c++-*-
// -------------------------------------------
// RCS data:
// $Date: 2003/07/04 09:48:21 $
// $Revision: 1.2 $
// $Source: /cvsroot/miwm/miwm/miwm/ws.h,v $
// $Id: ws.h,v 1.2 2003/07/04 09:48:21 sgbeal Exp $
// $RCSfile: ws.h,v $
//
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------

#ifndef WS_H
#define WS_H
// -------------------------------------------

#include <string>
#include "miwm.h"

// -------------------------------------------


class WorkSpace {
public:
  WorkSpace(const char* );
  ~WorkSpace();

  std::string label;

protected:
private:
};

// -------------------------------------------
#endif
// -------------------------------------------
// end of ws.h
// -------------------------------------------
