#ifndef ESTDSTRINGTOKENIZER_H
#define ESTDSTRINGTOKENIZER_H

#include <string>
#include <queue>

        using std::string;
        /**
           EStdStringTokenizer:

           License: see the file LICENSE in this directory.
           Author: stephan@wanderinghorse.net
           
           Based heavily off of work by:
           
           Martin Jones (mjones@kde.org)
           Torben Weis (weis@kde.org)
	   Waldo Bastian (bastian@kde.org)

           which i originally found as StringTokenizer in the KDE 1.x
           source tree. i have received explicit permission from each
           of those gentlemen to release the StringTokenizer code into
           into the Public Domain. (Many thanks to them for that
           permission, without which this whole library would
           necessarily be released under the GNU GPL.)

	   This class is meant to be API- and behaviour-compatible
           with StringTokenizer. This implementation is, however, MUCH
           less efficient.
           
           EStdStringTokenizer tokenizes strings in a way which is
           consistent with the way a Unix shell does. This makes it
           appropriate for use in parsing many types of arbitrary user
           input, from command-line arguments to comma-separated
           files.
        */
        class EStdStringTokenizer
        {
        public:
                EStdStringTokenizer();
                ~EStdStringTokenizer();

                /**
                   str is split up at points matching any element in
                   separators. Adjecent separators in str are
                   interpreted as empty elements. Thus the string
                   "1;;3", separated by ";", has 3 tokens:
                   ("1","","3").

                   To collect the tokens, do this:

                   EStdStringTokenizer tok( "some string", " " );
                   while( tok.hasMoreTokens() ) cout << "Token: " << tok.nextToken() << endl;
                 */
                void tokenize( const string &str, const string &separators );
                /**
                   Returns the next token in our list.
                   Calling nextToken() when hasMoreTokens() returns
                   false has undefined behaviour.
                 */
                string nextToken();
                /**
                   Returns true if this object has more tokens to give you.
                */
                bool hasMoreTokens() const;

        private:
                typedef std::queue<std::string> queue_type;
                //EStringList m_list;
                queue_type m_list;
        };

#endif // ESTDSTRINGTOKENIZER_H

