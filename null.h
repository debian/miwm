//-*-c++-*-
// -------------------------------------------
// RCS data:
// $Date: 2003/06/23 14:47:23 $
// $Revision: 1.1.1.1 $
// $Source: /cvsroot/miwm/miwm/miwm/null.h,v $
// $Id: null.h,v 1.1.1.1 2003/06/23 14:47:23 bwise837 Exp $
// $RCSfile: null.h,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------
// short comments explaining this file go here.
// 'null.h' is just a template for real files
// -------------------------------------------

#ifndef NULL_H
#define NULL_H

// -------------------------------------------

#include "miwm.h"

// -------------------------------------------



// -------------------------------------------
#endif
// -------------------------------------------
// end of null.h
// -------------------------------------------
