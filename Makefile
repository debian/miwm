# ----------------------------------------------------------------
# RCS data:
# $Date: 2003/07/08 13:33:18 $
# $Revision: 1.4 $
# $Source: /cvsroot/miwm/miwm/miwm/Makefile,v $
# $Id: Makefile,v 1.4 2003/07/08 13:33:18 bwise837 Exp $
# $RCSfile: Makefile,v $
#
# ----------------------------------------------------------------
# Copyright Ben Paul Wise.
# ----------------------------------------------------------------
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ----------------------------------------------------------------

default: all

DEPS_FILE = make.depend
include make.config
include $(DEPS_FILE)

# ----------------------------------------------------------------

# clear the list
.SUFFIXES:

# add our own list
.SUFFIXES: .o  .cc

CFLAGS = $(DBGFLAGS) -D_POSIX_SOURCE -DSHAPE


TARGET = all

OBJS = 	miwm.o manage.o mouse.o client.o cursor.o error.o	\
	disp.o shape.o resource.o  wspace.o wm.o menu.o	\
	null.o ops.o mipix.o				\
	alpha.o nodes.o rng.o mlchckr.o \
	miwm_framework.o \
	EKeyValueParser.o EPropertyList.o ESimpleCLParser.o EStringTokenizer.o EStringList.o EPathFinder.o EStdStringTokenizer.o \
	EShellProcess.o

HEADERS = client.h  miwm.h    struct.h	mimenu.h  null.h    ws.h mipix.h \
	miwm_framework.h \
	EKeyValueParser.h EPropertyList.h ESimpleCLParser.h EStringTokenizer.h EStringList.h EPathFinder.h EStdStringTokenizer.h \
	EShellProcess.h

SRCS = $(OBJS:.o=.cc)

INCS = 	-I.  -I/usr/X11R6/include -I/include	\
	-I$(INSTALLDIR)/include/beta
#	-I/usr/include/g++			\
#	-I$(HOME)/lib/beta/include

all: TAGS $(DEPS_FILE) miwm
#	$(MAKE) TAGS
#	$(MAKE) depend
#	$(MAKE) miwm

miwm:  $(OBJS) $(HEADERS)
	@echo linking $@
	$(CC) -g $(OBJS) -o miwm $(LDFLAGS)


# compile  CC  files
.cc.o :
	@echo "  compiling $*.cc"
	@$(CC)  -c $(CFLAGS) $(INCS) $*.cc -o $*.o



clean:
	rm -f *.o *~ core *.tmp

distclean:
	$(MAKE) clean
	rm -f TAGS *.pv .nfs*
	rm -f $(DEPS_FILE)
	rm -f make.config
	cp config.linux make.config
	rm -f miwm miwm.pure

TAGS:  $(SRCS) $(HEADERS)
	etags *.cc *.h


$(DEPS_FILE):  $(SRCS) $(HEADERS)
	@echo "Building $(DEPS_FILE) file. Please be patient."
	@$(CC) $(DEPFLAG) $(SRCS) $(INCS)  >> $(DEPS_FILE).tmp
	@mv $(DEPS_FILE).tmp $(DEPS_FILE)
	@echo "Done building $(DEPS_FILE) file."
depend: $(DEPS_FILE)

cleanly:
	$(MAKE) distclean
	$(MAKE) all
	$(MAKE) clean

# make sure, on install, that we are actually installing
# the most recent executable. also move a basic man page.
# NB: you probably have to be root to do this.
install:	
	$(MAKE) miwm
	cp miwm $(INSTALLDIR)/bin/miwm
	cp miwm.1 $(INSTALLDIR)/man/man1

MANIFEST: distclean
	ls -lR > MANIFEST; ls -lR > MANIFEST

# make compressed archive
zdistrib: MANIFEST
	cd ..; tar -czvf miwm`date +%Y%m%d`.tgz miwm

# make uncompressed archive
distrib: 
	@$(MAKE) distclean
	@$(MAKE) MANIFEST
	@echo making compressed archive
	@cd ..; tar -cf miwm`date +%Y%m%d`.tar miwm

# make a distribution, sign it, and bundle them together
# (no need to compress twice)
signed_zdistrib: MANIFEST
	make distrib
	cd .. ; gpg --armor --output miwm`date +%Y%m%d`.tar.sig  --detach-sign miwm`date +%Y%m%d`.tar
	cd .. ; tar -zcf miwm`date +%Y%m%d`.tgz    miwm`date +%Y%m%d`.tar.sig   miwm`date +%Y%m%d`.tar
	cd .. ; rm -f  miwm`date +%Y%m%d`.tar.sig   miwm`date +%Y%m%d`.tar

# ----------------------------------------------------------------
# end of miwm makefile
# ----------------------------------------------------------------
