//-*-c++-*-
// -------------------------------------------
// RCS data:
// $Date: 2003/07/03 20:12:44 $
// $Revision: 1.2 $
// $Source: /cvsroot/miwm/miwm/miwm/mimenu.h,v $
// $Id: mimenu.h,v 1.2 2003/07/03 20:12:44 sgbeal Exp $
// $RCSfile: mimenu.h,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------
// very simple menu-ing. specialized to miwm.
// -------------------------------------------

#ifndef MIMENU_H
#define MIMENU_H

// -------------------------------------------
#include <string>
#include "miwm.h"

class Menu;
class MenuItem;
// -------------------------------------------

class Menu {
 public:
  Menu();
  ~Menu();

  void startMenuing(XButtonEvent* xbe);
  void motionNotify(XEvent* xe);
  void buttonRelease();
  void expose();
  int whichMenuItem();


  // things to do the the data of each item
  void  (*enterAction)(void*);
  void (*exitAction)(void*);
  void (*releaseAction)(void*);

  void raise();
  void lower();

  NodeList* items; // data of type MenuItem
  void setDimensions();

  int currItem; // you should set this at creation time

 protected:
  int prevItem; // will become the same at currItem, at the start
  MenuItem *currMI;
  MenuItem *prevMI;
  int length; // number of items
  int x; // root coords
  int y; // root coords
  int mWidth; // menu width
  int mHeight; // menu height
  int iWidth; // item width
  int iHeight; // item height

 private:

};


class MenuItem {
public:
  MenuItem();
  MenuItem(const char*, const void *);
  ~MenuItem();

  std::string label;
  const void* data; // Client*, WorkSpace*, etc.

protected:

private:

};
// -------------------------------------------
#endif
// -------------------------------------------
// end of mimenu.h
// -------------------------------------------
