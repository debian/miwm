
#include "EStdStringTokenizer.h"
//#include <iostream> // only for debuggering

        enum quoteEnum { NO_QUOTE=0, SINGLE_QUOTE, DOUBLE_QUOTE };
        /**
           todo: implement a map<char,int> to count opening/closing quotes so i can add support
           for open/close { } without having to add to the enum, add if(), etc.
           map['\''] = 0. increment on an opener, decrement on a closer.
        */


        EStdStringTokenizer::EStdStringTokenizer()
        {
        }

        void EStdStringTokenizer::tokenize( const string &str, const string & sep )
        {
                const string::size_type ssize = str.size();
                if ( ssize == 0 ) return;
                if ( ssize == 1 )
                {
                        this->m_list.push( str ); //  += str;
                        return;
                }
                if( string::npos == str.find_first_of( sep ) )
                {
                        this->m_list.push( str ); // += str;
                        return;
                }

                int quoted = NO_QUOTE;
                char chat;
                string mystr;
                bool addit;
                for( string::size_type pos = 0; (pos < ssize); pos++ )
                {
                        chat = str[pos];
                        addit = true;
                        if ( (chat == '\"' ) && !quoted)
                        {
                                quoted = DOUBLE_QUOTE;
                                addit = false;
                        }
                        else if ( ( chat == '\'') && !quoted) 
                        {
                                quoted = SINGLE_QUOTE;
                                addit = false;
                        }
                        else if ( ( (chat == '\"') && (quoted == DOUBLE_QUOTE) )
                                  ||
                                  ( (chat == '\'') && (quoted == SINGLE_QUOTE) ) )
                        {
                                quoted = NO_QUOTE;
                                addit = false;
                        } 
                        
                       if ( !quoted ) 
                       {
                                for( string::size_type i = 0; i < sep.size(); i++ )
                                {
                                        if( chat == sep[i] )
                                        {
                                                m_list.push( mystr ); // += mystr;
                                                mystr = string();
                                                addit = false;
                                                break;
                                        }
                                }
                        }

                        if( addit ) mystr += chat;
                }
                if( ! mystr.empty() ) m_list.push( mystr ); // += mystr;
                return;
        }

        bool
        EStdStringTokenizer::hasMoreTokens() const
        {
                return ! this->m_list.empty();
        }
        std::string
        EStdStringTokenizer::nextToken()
        {
                string foo = this->m_list.front();
                this->m_list.pop();
                return foo;
        }

        EStdStringTokenizer::~EStdStringTokenizer()
        {
        }

