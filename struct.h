//-*-c++-*-
// -------------------------------------------
// RCS data:
// $Date: 2003/06/23 14:47:24 $
// $Revision: 1.1.1.1 $
// $Source: /cvsroot/miwm/miwm/miwm/struct.h,v $
// $Id: struct.h,v 1.1.1.1 2003/06/23 14:47:24 bwise837 Exp $
// $RCSfile: struct.h,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------

#ifndef STRUCT_H
#define STRUCT_H

// -------------------------------------------

#include "miwm.h"

class WorkSpace;
class Menu;
class MenuItem;
class WindowManager;

// -------------------------------------------

typedef void (*WMOperation)(void);

// this is a simple list of (label, Client* , int) data.
// if the Client is non-null, then it refers to that client.
// if the client is null and the int is non-negative, it refers to that workspace.
typedef struct menuitem menuitem;

typedef struct Client Client;

// this is a menu of hidden clients or workspace names
// (if the client is 0, then it is just a workspace name)
struct menuitem {
  char * label;            // name to be displayed
  Client * client;        // client window (if any!)

// THIS IS NOT THE WORKSPACE OF THE CLIENT!
  int space;               // work space (if any!) 

  menuitem * next;   // next in list
};



enum BoxStatus {
  DBunarmed, DBarmed, DBundrawn
};

enum Mode { // Window Manager modes
  wmStartup, 
  wmShutdown, 
  wmReady,  // the base mode
  wmResizing, 
  wmMoving,
  wmKilling,
  wmMenuing
};

// the menu can be raised several ways.
// this list will expand ....
enum menuType {
  noMenu, 
  operationsMenu,    // perform various built-in operations
  hiddenClientMenu,           // hidden clients in WS
  allClientsMenu,                  // all clients in WS
  commandMenu,                   // user-defined shell commands we can run
  changeWSMenu,    // choice of WS
  sendCtoWSMenu    // choice of WS
};

//	Window internal state. Yuck.

typedef enum {
    IPendingReparenting, INormal
} IState ;


// Window edge, used in resizing. The `edge' ENone is used to signify a
//  window move rather than a resize. The code is sufficiently similar that
//  this isn't a special case to be treated separately.

//  These MUST be in the same order as they appear in cursor.cc !

typedef enum {
    ETopLeft = 0,	ETop =1,		ETopRight=2,
    ERight =3,		ENone =4,		ELeft =5,
    EBottomLeft =6,	EBottom =7,	EBottomRight =8
} Edge ;


// 	c->proto is a bitarray of these
enum {
    Pdelete = 1,
    Ptakefocus = 2
};





// -------------------------------------------
#endif
// -------------------------------------------
// end of struct.h
// -------------------------------------------
