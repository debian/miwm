//-*-c++-*-
// -------------------------------------------
// RCS data:
// $Date: 2003/06/23 14:47:23 $
// $Revision: 1.1.1.1 $
// $Source: /cvsroot/miwm/miwm/miwm/mipix.h,v $
// $Id: mipix.h,v 1.1.1.1 2003/06/23 14:47:23 bwise837 Exp $
// $RCSfile: mipix.h,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------

#ifndef MIPIX_H
#define MIPIX_H

// -------------------------------------------

#include "miwm.h"

#ifdef USE_PIXMAPS
#include <X11/xpm.h>
#include "wood.xpm"
#include "greenstucco.xpm" 
#include "sunflower.xpm" 
#include "back.xpm" 
#endif

// -------------------------------------------

void loadPixmap(char **dname, Pixmap *xpm);


// -------------------------------------------
#endif
// -------------------------------------------
// end of mipix.h
// -------------------------------------------
