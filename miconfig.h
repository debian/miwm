//-*-c++-*-
// -------------------------------------------
// RCS data:
// $Date: 2003/06/23 14:47:23 $
// $Revision: 1.1.1.1 $
// $Source: /cvsroot/miwm/miwm/miwm/miconfig.h,v $
// $Id: miconfig.h,v 1.1.1.1 2003/06/23 14:47:23 bwise837 Exp $
// $RCSfile: miconfig.h,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------
// configurable defaults. these need adjustment
// from system to system and person to person
// -------------------------------------------

#ifndef MICONFIG_H
#define MICONFIG_H

// -------------------------------------------

#include "miwm.h"


const int maxNumWorkSpaces = 100; // anything beyond this is ridiculous


// -------------------------------------------

// defining this will cause verbose event-logging
// #define MIWM_TRACE_ALL_X_EVENTS

// my preferences
#define DEFAULT_TITLE_FONT  "-*-lucida-bold-r-normal-sans-12-*-*-*-p-*-iso8859-1"
#define DEFAULT_POPUP_FONT  "-*-lucida-medium-r-normal-sans-10-*-*-*-p-*-iso8859-1"

// if you are desperate for screen space, this helps (but is ugly)
//  #define DEFAULT_TITLE_FONT   "6x9"
//  #define DEFAULT_POPUP_FONT "6x9"

// define this to make background pixmaps possible.
// they just look  like a weak attempt to imitate
// Enlightenment. Perhaps with better pixmaps they would
// look decent ...
#define USE_PIXMAPS


 // I don't use this, and it wastes space
// #define SPECIAL_POPUP_FONT
//  "-*-lucida-medium-i-normal-sans-10-*-*-*-p-*-iso8859-1"



#define DEFAULT_TERMINAL "rxvt"
#define DEFAULT_BORDER 6
#define DEFAULT_SEPARATION 3

// number of workspaces is set in setupWS();

// undefine this to shut off client previewing, define to enable
// #define CLIENTPREVIEWING

// undefine this to shut off workspace previewing, define to enable
// #define WORKSPACEPREVIEWING

// maximum string length of a line in .miwmrc
const int MaxShellCommandLength = 200;

// do not make the active border color be exactly black.
// if you do, then client outlines will be invisible during move/resize
// because of the xor-operation.
//
// Nice default settings:
//
// #define ROOT_CURSOR_COLOR "blue"
// #define FRAME_CURSOR_COLOR "darkcyan"
// #define ACTIVE_BORDER_COLOR "grey30"
// #define INACTIVE_BORDER_COLOR "beige"
// #define TITLE_COLOR "cyan"
// #define DEADLY_BOX_COLOR "red3"


// because of one-pixel difference, there are tiny borders
// both inside and out. If you reverse the active/inactive color
// scheme so that active is bright and inactive is dark, then it is
// quite apparent
//  #define ACTIVE_BORDER_COLOR "rgb:10/50/50"

// grey10 is nearly pure black, and grey20
// shows up on a pure black background

#define ROOT_CURSOR_COLOR "gold"
#define FRAME_CURSOR_COLOR "darkcyan"
#define ACTIVE_BORDER_COLOR "grey20"
#define INACTIVE_BORDER_COLOR "beige"
#define TITLE_COLOR "gold"
#define DEADLY_BOX_COLOR "red3"

// -------------------------------------------
#endif
// -------------------------------------------
// end of miconfig.h
// -------------------------------------------
