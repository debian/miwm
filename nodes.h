//-*-c++-*-
// ------------------------------------------
// RCS data:
// $Date: 2003/06/23 14:47:23 $
// $Revision: 1.1.1.1 $
// $Source: /cvsroot/miwm/miwm/miwm/nodes.h,v $
// $Id: nodes.h,v 1.1.1.1 2003/06/23 14:47:23 bwise837 Exp $
// $RCSfile: nodes.h,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------
// handle specialized lists. not suitable
// for generic use.
// -------------------------------------------
//
// notice that we have to be careful with aggregate
// data types like int, float, etc. because the
// pointer-comparision of equivalence will fail
// to find equivalent data. see the demo program.
//
// You also have to be careful in 'delete' of nodes'
// contents that those contents were actually created
// with 'new'. This is a problem in the window manager,
// where X creates the client structures with malloc,
// but then I try to clean up with 'delete'. My 
// memory leak checker catches this subtle error.
//
// ------------------------------------------

#ifndef NODE_LIST_H
#define NODE_LIST_H

#include "alpha.h"
#include <stddef.h>
// #include <assert.h>
// #include <stdio.h>
// #include <iostream.h>

// ------------------------------------------
class NodeList;
class Node;

// delete each node's contents, then delete the list itself (and its nodes)
void obliterateNodeList(NodeList*);

// ------------------------------------------

// clashed with Xlib.h
//
// #ifndef False
// #define False 0
// #endif
// #ifndef True
// #define True (-1)
// #endif


// ------------------------------------------

class Node {
public:
  Node();
  ~Node();

  Node *next;
  Node *prev;
  
  void *data;

protected:
private:
};


// ------------------------------------------

class NodeList {
public:
  NodeList();
  ~NodeList();

  Node *first;
  Node *last;
  Node *actual;


  Node* inList(void* data);
  TLogical prepend(void* data);
  TLogical append(void* data);
  int length();
  Node* at(int i);
  Node* remove(Node* node);
  Node* nextNode(Node* node);
  Node* prevNode(Node* node);

  void moveToEnd(Node* node);
  void moveToFront(Node* node);
protected:
private:
};



// ------------------------------------------
#endif
// ------------------------------------------
// end of nodes.h
// ------------------------------------------
