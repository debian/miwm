//-*- c++ -*-
// -------------------------------------------
// RCS data:
// $Date: 2003/06/23 14:47:22 $
// $Revision: 1.1.1.1 $
// $Source: /cvsroot/miwm/miwm/miwm/error.cc,v $
// $Id: error.cc,v 1.1.1.1 2003/06/23 14:47:22 bwise837 Exp $
// $RCSfile: error.cc,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------

#include	<stdio.h>
#include	<stdlib.h>

#include	<X11/X.h>
#include	<X11/Xlib.h>
#include	<X11/Xutil.h>
#include	<X11/Xproto.h>

#include	"miwm.h"

int ignore_badwindow;

void
miAbort(char *s)
{
  fprintf(stderr, "%s: %s\n", argv0, s);
  cout << endl;
  cout << "Terminating MIWM at ";
  cout << __TIME__<<", "<<__DATE__<<endl<<flush;

  StopMemoryLeakCheck();

  exit(EXIT_FAILURE);
}

int
ErrorHandler(Display *d, XErrorEvent *e)
{
  char	msg[80];
  char	req[80];
  char	number[80];

  if (theWM->mode == wmStartup &&
      e->request_code == X_ChangeWindowAttributes &&
      e->error_code == BadAccess)
    miAbort("another window manager is already running.");

  if (ignore_badwindow &&
      (e->error_code == BadWindow || e->error_code == BadColor))
    return 0;

  XGetErrorText(d, e->error_code, msg, sizeof(msg));
  sprintf(number, "%d", e->request_code);
  XGetErrorDatabaseText(d, "XRequest", number, number, req, sizeof(req));

  fprintf(stderr, "%s: protocol request %s on resource %#x failed: %s\n\n",
	  argv0, req, (unsigned int) e->resourceid, msg);

  if (theWM->mode == wmStartup)
    miAbort("Failed during initialization.");

  return 0;
}

// -------------------------------------------
// end of error.cc
// -------------------------------------------
