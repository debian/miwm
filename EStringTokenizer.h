//   This file was part of the KDE libraries
//    Copyright (C) 1997 Martin Jones (mjones@kde.org)
//              (C) 1997 Torben Weis (weis@kde.org)
//              (C) 1998 Waldo Bastian (bastian@kde.org)
//
//    This copy was then part of the QUB project.
//    Copyright (C) 2000-2003 stephan@wanderinghorse.net
//
//   Since then i have received permission from Martin, Torben and Waldo
//   to release this code into the Public Domain, and that is the license
//   for this code.
//
// ***************************************************************************

#ifndef STRINGTOKENIZER_H
#define STRINGTOKENIZER_H


class EStringTokenizer
{
public:
    EStringTokenizer();
    ~EStringTokenizer();

    void tokenize( const char *, const char * );
    const char* nextToken();
    bool hasMoreTokens() { return ( pos != 0 ); }

protected:
    char *pos;
    char *end;
    char *buffer;
    int  bufLen;
};

#endif // STRINGTOKENIZER_H

