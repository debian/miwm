//-*-c++-*-
// -------------------------------------------
// RCS data:
// $Date: 2003/06/23 14:47:22 $
// $Revision: 1.1.1.1 $
// $Source: /cvsroot/miwm/miwm/miwm/client.h,v $
// $Id: client.h,v 1.1.1.1 2003/06/23 14:47:22 bwise837 Exp $
// $RCSfile: client.h,v $
// -------------------------------------------
// Copyright by Ben Paul Wise.
// -------------------------------------------
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// -------------------------------------------

#ifndef CLIENT_H
#define CLIENT_H

// -------------------------------------------

#include "miwm.h"
class WorkSpace;

// -------------------------------------------

struct Client {
// public:
//   Client();
//   ~Client();

  Window window;		// Client's window. 
  Window parent;		// Window manager frame. 
  Window trans;			// Window that client is a transient for. 

  int clientID;

  Client * next;			// Next window in client list. 

  int border;			// Client's original border width. 

  XSizeHints size;		// Client's cUrrent geometry information. 
  int state;				// Window state. See ICCCM and <X11/Xutil.h> 

  Bool hidden;			// True if this client is hidden by miwm. 
  int isMapped;                                      // 1 if mapped by X, 0 otherwise
  IState internal_state;
  int proto;

  int accepts_focus;		// Does this window want keyboard events? 

  char * name;			// Name used for title in frame. 
  char * fallback_name;	// Name used in menu when window has no titlebar. 

  BoxStatus boxStatus;

  Colormap cmap;
  int ncmapwins;
  Window * cmapwins;
  Colormap * wmcmaps;

  // to which workspace does this belong
  // (i.e. if you suddenly make it unsticky, it will appear in this one)
  WorkSpace* workSpace; 
  int normalX;
  int normalY;
  int normalW;
  int normalH;

  int sticky;  // does it appear in all


  // WM data on the move/resize under way:
  int opaqueMoveP;

  int sX; // starting client X (since actually moved/resized)
  int sY;
  int sW;
  int sH;
  int sBX; // starting button X  (since actually moved/resized)
  int sBY;

  int rX; // recent client X
  int rY;
  int rW;
  int rH;
  int rBX; // recent button X
  int rBY;

  Edge gripEdge;
  int outlinedP;

// protected:
// private:
};



// -------------------------------------------
#endif
// -------------------------------------------
// end of client.h
// -------------------------------------------
