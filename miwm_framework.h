#ifndef MIWM_FRAMEWORK_H
#define MIWM_FRAMEWORK_H 1

#include "EPropertyList.h"
#include "ESimpleCLParser.h"
#include <iostream>

/**
COUT/CERR are drop-in replacements for std::cout/cerr, but slightly more
decorative.
*/
#ifndef CERR
#define CERR std::cerr << __FILE__ << ":" << std::dec << __LINE__ << " : "
#endif
#ifndef COUT
#define COUT std::cout << __FILE__ << ":" << std::dec << __LINE__ << " : "
#endif


namespace miwm {

        /**
           Call this from main() to initialize the miwm framework.
        */
        int init( int argc, char ** argv );
        /**
           Shows command-line args help.
         */
        void show_help();

        /**
           returns an absolute path to a directory, which is the
           "home" of miwm.  Currently this returns $HOME, but i would
           prefer for it to point to $HOME/.miwm. It will, once i get
           around to adding portable code for creating ~/.miwm if it
           doesn't exist.
         */
        std::string homePath( const std::string & append = "" );

        /**
           Returns the command-line args, which are only valid if
           init() is called with the argc and argv from main().
         */
        ESimpleCLParser & args();

        /**
           Returns the persistant framework config settings.
         */
        EPropertyList & config();


}; // namespace miwm

#endif // MIWM_FRAMEWORK_H
